# -*- coding: utf-8 -*-
from lyra import *
class Declarats:
    def __init__(self):
        self.functions = []
        self.integers = []
        self.strings = []
        self.floats = []
        self.lists = []
        self.globes = []
        self.functionNames = []

class Frame:
    def __init__(self, parent):
        self.locals = {}
        self.parent = parent
        self.top = self
        while self.top.parent != None:  # tohle najde globalni frame, nenapadlo me lepsi reseni
            self.top = self.top.parent

    def set(self, name, value):
        self.locals[name] = value

    def get(self, name):
        if (name in self.locals):
            return self.locals[name]
        if (self.parent == None):
            raise SyntaxError("promena '" + name + "' nebyla nalezena")
        return self.parent.get(name)


class Block:
    """ Blok prikazu.

    Nejedna se o nic jineho, nez o seznam prikazu, ktere jdou po sobe.
    """

    def __init__(self):
        self.code = []

    def add(self, node):
        """ Prida novy prikaz. """
        self.code.append(node)

    def __str__(self):
        result = "{"
        for node in self.code:
            result = result + "\n" + str(node.__str__())
        result = result + "\n}\n"
        return result

    def __repr__(self):
        return self.__str__()

    def exe(self, frame):
        # tady by se asi vic hodil while, ale nevim jak to napsat tak aby to fungovalo se zbytkem
        for node in self.code:
            node.exe(frame)




class BinaryOperator:
    """ Binary operator.

    Pamatuje si levy a pravy operand a typ operace, kterou s nimi ma provest. """
    def __init__(self, left, right, operator):
        self.left = left
        self.right = right
        self.operator = operator

    def __str__(self):
        return "( %s %s %s)" % (self.left, self.operator, self.right)

    def __repr__(self):
        return self.__str__()

    def getType(self, frame):
        lhsType = self.left.getType(frame)
        rhsType = self.right.getType(frame)
        if (lhsType == "INT" and rhsType == "INT"):
            return "INT"





    def exe(self, frame):

        l = self.left.exe(frame)
        r = self.right.exe(frame)
        if isinstance(l,int) and isinstance(r,int):
            if self.operator == Lexer.OP_ADD:
                return l + r
            elif self.operator == Lexer.OP_SUB:
                return l - r
            elif self.operator == Lexer.OP_MULTIPLY:
                return l * r
            elif self.operator == Lexer.OP_DIVIDE:
                return l / r
            elif self.operator == Lexer.OP_CUBE:
                return l ** r
            # par operatoru porovnavani, meli by vracet true nebo false
            elif self.operator == Lexer.OP_BIGGER: # z nejakeho duvodu > a < funguji obracene, obratil sem je zde znovu
                return l < r
            elif self.operator == Lexer.OP_SMALLER:
                return l > r
            elif self.operator == Lexer.OP_EQ:
                return l == r
        if isinstance(l,list) and isinstance(r,int):
            if self.operator == Lexer.OP_ADD:
                l = l + [r]
                return l
            elif self.operator == Lexer.OP_SUB:
                return l.pop(r)
            elif self.operator == Lexer.OP_MULTIPLY:
                a = []
                for i in range(r):
                    a += l
                return a
            elif self.operator == Lexer.OP_DIVIDE:
                return l.remove(r)
        if isinstance(l, list) and isinstance(r, list):
            if self.operator == Lexer.OP_ADD:
                return l + r

        else:
            raise SyntaxError("Nepovolena kombinace napr cislo+pismeno: " + str(l) + " - " + str(l.__class__.__name__) + " " + str(r) + " - " + str(r.__class__.__name__) + ", operator je " + str(self.operator))

class UnaryOperator:
    def __init__(self, right, operator):
        self.right = right
        self.operator = operator

    def __str__(self):
        return "(%s %s)" % ( self.operator[0], self.right)

    def __repr__(self):
        return self.__str__()

    def exe(self, frame):
        return -(self.right.exe(frame))

class VariableRead:
    """ Cteni hodnoty ulozene v promenne. """
    def __init__(self, variableName):
        self.variableName = variableName

    def __str__(self):
        return self.variableName

    def __repr__(self):
        return self.__str__()

    def exe(self, frame):
        return frame.get(self.variableName)

class VariableWrite:
    """ Zapis hodnoty do promenne. Krom nazvu promenne si pamatuje i vyraz, kterym se vypocita hodnota. """
    def __init__(self, variableName, value):
        self.variableName = variableName
        self.value = value

    def __str__(self):
        return "%s = %s" % (self.variableName, self.value)

    def __repr__(self):
        return self.__str__()

    def exe(self, frame):
        x = self.value.exe(frame)
        print str(self.variableName), x
        if self.variableName in program.globes:
            frame.top.set(self.variableName, x)
        frame.set(self.variableName, x)

class Literal:
    """ Literal (tedy jakakoli konstanta, cislo). """
    def __init__(self, value):
        self.value = int(value)

    def __str__(self):
        return str(self.value)

    def __repr__(self):
        return self.__str__()

    def exe(self, frame):
        return self.value

class List:
    #seznamy
    def __init__(self,list):
        self.list = list

    def __str__(self):
        x = "["
        for i in self.list:
            x += str(i)+", "
        x += "]"
        return x

    def exe(self, frame):
        return self.list

class ListRead:
    def __init__(self, list, index):
        self.listname = list
        self.index = index

    def exe(self, frame):
        temp = frame.get(self.listname)[self.index.exe(frame)]
        if isinstance(temp, int):  # jasne, spatny reseni, ale rychly
            return temp
        return temp.exe(frame)


class If:
    """ Prikaz if. Pamatuje si vyraz ktery je podminkou a pak bloky pro true a else casti. """
    def __init__(self, condition, trueCase, falseCase):
        self.condition = condition
        self.trueCase = trueCase
        self.falseCase = falseCase

    def __str__(self):
        return "if (%s) %s else %s" % (self.condition, self.trueCase, self.falseCase)

    def exe(self, frame):
        if self.condition.exe(frame):#tohle se proste satne spatne, nikdy se to nedostane do BinaryOperator, jen to rekne true
            self.trueCase.exe(frame)
        elif not self.condition.exe(frame):
            self.falseCase.exe(frame)
        else:
            raise StandardError("ocekaval sem True nebo False, dostal sem" + str(self.condition.exe))

class While:
    def __init__(self, condition, block):
        self.condition = condition
        self.block = block

    def __str__(self):
        return "while " + str(self.condition) + str(self.block)

    def exe(self, frame):
        while self.condition.exe(frame):
            for node in self.block.code:
                node.exe(frame)

class Function:
    def __init__(self, name, block, params):
        self.name = name
        self.block = block
        self.params = params

    def __str__(self):
        return "func " + str(self.name) + str(self.block)

    def __repr__(self):
        return self.name

    def exe(self, frame):
        self.block.exe(frame)

class ReturnException:
  def __init__(self, value):
    self.value = value

class Return:
    def __init__(self, value):
        self.value = value

    def exe(self, frame):
        raise ReturnException(self.value)

    def __str__(self):
        return "return", self.value

class Functioncall:
    def __init__(self, name, params):
        self.name = name
        self.params = params
    def exe(self, frame):
        x = None
        self.frame = Frame(frame)
        for i in program.functions:
            if i.name == self.name:
                for j in range(len(self.params.list)):
                    self.frame.set(str(i.params.exe(frame)[j]), self.params.exe(frame)[j].exe(frame))
                try:
                    i.exe(self.frame)
                except ReturnException as e:
                    return e.value.exe(self.frame)
            # funkce nenalezena

    def __str__(self):
        return self.name
program = Declarats()