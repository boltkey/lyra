# -*- coding: utf-8 -*-
from ast import *


class Parser:
    """ Jednoduchy priklad parseru. Naparsuje nasledujici gramatiku:

    BLOCK ::= Ä‚ËĂ˘â€šÂ¬Ă‹Ĺ›{Ä‚ËĂ˘â€šÂ¬Ă‹Ĺ› { STATEMENT } Ä‚ËĂ˘â€šÂ¬Ă‹Ĺ›}Ä‚ËĂ˘â€šÂ¬Ă˘â€žË
    PROGRAM ::= { STATEMENT }
    COMPARISON ::= (kw_bigger | kw_eq | kw_smaller)
    STATEMENT ::= { EXPRESSION | CONDITION | LOOP }
    EXPRESSION ::= E1 { op_assign E1 }
    E1 ::= E2 { ( op_add | op_sub |  op_divide | op_cube | op_divide | op_mulitply ) E2 }
    E2 ::= [ op_sub ] F
    F ::= ident | number | par_open EXPRESSION par_close
    CONDITION ::= kw_if Ä‚ËĂ˘â€šÂ¬Ă‹Ĺ›)Ä‚ËĂ˘â€šÂ¬Ă˘â€žË  EXPRESSION COMPARISON EXPRESSION  Ä‚ËĂ˘â€šÂ¬Ă‹Ĺ›(Ä‚ËĂ˘â€šÂ¬Ă‹Ĺ› BLOCK [ kw_else BLOCK ]
    LOOP ::= kw_while Ä‚ËĂ˘â€šÂ¬Ă‹Ĺ›)Ä‚ËĂ˘â€šÂ¬Ă˘â€žË EXPRESSION COMPARISON EXPRESSION Ä‚ËĂ˘â€šÂ¬Ă‹Ĺ›(Ä‚ËĂ˘â€šÂ¬Ă‹Ĺ› BLOCK

    Mimochodem delame jazyk ktery nedodrzuje matematickou posloupnost operaci, aneb nasobeni NEma prednost pred scitanim atd.

    Znacnou cast kodu vubec nechapu delali jsem ve skole neco co vypadalo o dost jednoduseji.

    Ten parser se trosku lisi od toho parseru, ktery jsme delali na hodine, protoze uz pouziva ty jednoduzsi stromy,
     o kterych jsem mluvil. Takze misto toho, aby kazde pravidlo melo svoji vlasti tridu (v ast), tak ty tridy pro
     ten strom odpovidaji spis tomu, co ten program bude delat, nez tomu, jak je napsana gramatika. Blizsi popisky
     viz jednotlive parsovaci metody nize.
    """


    def __init__(self, lexer):
        """ Zalozi parser a zapamatuje si lexer, se kterym bude pracovat. """
        self.lexer = lexer

    def pop(self, type = None):
        """ Abych nemusel tolik psat, provede pop z lexeru. Kdyz specifikuju druh tokenu, ktery ocekavam, tak zaroven
        zkontroluje jestli je to spravny typ, a vyhodi vyjimku, kdyz tomu tak neni.
        """
        if (type is None):
            return self.lexer.popToken()
        t = self.lexer.topToken()
        if (t[0] == type):
            return self.lexer.popToken()
        else:
            raise SyntaxError("Ocekavany token ( " + type + " ) neni na vstupu, misto nej ( " + t[0] + " ) ,cislo tokenu "+ str(self.lexer._top))

    def top(self, ahead=0):
        """ Abych nemusel tolik psat, provede top() z lexeru.
        """
        return self.lexer.topToken(ahead=ahead)

    def parse(self):
        """ Hlavni metoda na parsovani, odpovida pravidlu pro program:

        PROGRAM ::= { STATEMENT }

        Z hlediska stromu (ast) je program tedy blok prikazu, postupne jak prikazy parsuju tak je pridavam do toho bloku.
         Kdyz zjistim, ze uz na vstupu zadny token nemam, tak jsem skoncil.
        """
        program = Block()
        while (self.top()[0] != Lexer.EOF):
            program.add(self.parseStatement())
        return program



    def parseStatement(self):
        """ STATEMENT ::= IF_STATEMENT | ASSIGNMENT
        !!! STATEMENT ::= { EXPRESSION | CONDITION | LOOP } !!!
        Statement je bud if, nebo zapis promenne.
        """
        #print self.lexer._top
        if (self.top()[0] == Lexer.KW_IF):
            return self.parseCondition()
        elif (self.top()[0] == Lexer.KW_WHILE):
            return self.parseLoop()
        elif (self.top()[0] == Lexer.KW_FUNC):
            a = self.parseDefFunction()
            if a.name in program.functions:
                program.functions.remove(a.name)
            program.functions.append(a)
            program.functionNames.append(str(a.name))
        elif (self.top()[0] == Lexer.KW_RETURN):
            return self.parseReturn()
        elif self.top()[0] == Lexer.KW_INT:
            self.pop(Lexer.KW_INT)
            a = self.pop()[1]
            program.integers.append(a)
        elif self.top()[0] == Lexer.KW_STR:
            self.pop(Lexer.KW_STR)
            a = self.pop()[1]
            program.strings.append(a)
        elif self.top()[0] == Lexer.KW_FLT:
            self.pop(Lexer.KW_FLT)
            a = self.pop()[1]
            program.floats.append(a)
        elif self.top()[0] == Lexer.KW_LST:
            self.pop(Lexer.KW_LST)
            a = self.pop()[1]
            program.lists.append(a)
        elif self.top()[0] == Lexer.KW_GLOBAL:
            self.pop(Lexer.KW_GLOBAL)
            a = self.pop()[1]
            program.globes.append(a)
        else:
            name = self.top()[1]
            if self.top() == Lexer.OP_PAROPEN:
                return self.parseCallFunction()
            return self.parseAssignment()

    def parseAssignment(self):
        """ ASSIGNMENT ::= ident op_assign EXPRESSION
        !!! Sice v nasi gramatice Assingment neni ale da se vyuzit !!!
        Ulozeni hodnoty do promenne vypada tak, ze na leve strane je identifikator promenne, hned za nim je operator prirazeni a za nim je vyraz, ktery vypocitava hodnotu, kterou do promenne chci ulozit. Tohle je zjednodusena verze prirazeni, viz komentare k hodine.
        """
        variableName = self.pop(Lexer.IDENT)[1]
        self.pop(Lexer.OP_ASSIGN)
        rhs = self.parseE1()
        return VariableWrite(variableName, rhs)

    def parseReturn(self):
        self.pop(Lexer.KW_RETURN)
        rhs = self.parseE1()
        return Return(rhs)



    def parseE1(self):
        """ E2 ::= F { op_add | op_sub F }
        !!! E1 ::= E2 { ( op_add | op_sub | op_cube | op_divide | op_mulitply ) E2 } !!!
        Druhy level priorit je scitani a odcitani. Opet se jedna o binarni operatory.
        Tak tady trochu nechapu, proc kdyz v gramatice neni evedono E2 se s nim v kodu pracuje.
        """
        lhs = self.parseF()
        while (self.top()[0] in (Lexer.OP_ADD, Lexer.OP_SUB, Lexer.OP_DIVIDE, Lexer.OP_MULTIPLY, Lexer.OP_CUBE)):
            op = self.pop()[0]
            rhs = self.parseE2()
            lhs = BinaryOperator(lhs, rhs, op)
        return lhs

    def parseE2(self):
        """E2 ::= [ op_sub ] F
        Udelano podle vzoru parseE1. Vrati vyraz s minusem; minus je zatim nas jediny unarni operator
        """
        sub = False
        if self.top()[0] == Lexer.OP_SUB:
            sub = True
            op = self.pop()
        rhs = self.parseF()
        if sub:
            rhs = UnaryOperator(rhs, op)
        return rhs

    def parseF(self):
        """ F ::= number | ident | list | op_paropen EXPRESSION op_parclose | list item

        Faktorem vyrazu pak je bud cislo (literal), nebo nazev promenne, v tomto pripade se vzdycky jedna o cteni promenne a nebo znova cely vyraz v zavorkach.
        """
        if (self.top()[0] == Lexer.NUMBER):
            value = self.pop()[1]
            return Literal(value)
        elif (self.top()[0] == Lexer.IDENT):
            variableName = self.top()[1]
            if self.top(ahead=1)[0] == "(":
                return self.parseCallFunction()
            self.pop()
            if self.top()[0] == "[":
                self.pop()
                index = self.parseE1()
                self.pop()
                return ListRead(variableName, index)
            return VariableRead(variableName)
        elif (self.top()[0] == Lexer.OP_HZOPEN):
            return self.parseList()
        else:
            self.pop(Lexer.OP_PAROPEN)
            result = self.parseE1()
            self.pop(Lexer.OP_PARCLOSE)
            return result

    def parseList(self):
        """ List ::= '[' {F} ']'
        """
        list = []
        self.pop(Lexer.OP_HZOPEN)
        print self.top(), "$%%$%$%"
        while self.top()[0] != "]":
            list.append(self.parseE1())
            if self.top()[0] == Lexer.OP_HZCLOSE:
                break
            self.pop(",")
        self.pop(Lexer.OP_HZCLOSE)
        return List(list)


    def parseDefFunction(self):
        """
        Adam: naparsovani definice funkce, podobne bloku. David: To teda vubec.
        """
        self.pop(Lexer.KW_FUNC)
        name = self.pop(Lexer.IDENT)[1]
        self.pop("(")
        params = self.parseList()
        self.pop(")")
        block = self.parseBlock()
        return(Function(name, block, params))

    def parseCallFunction(self):  # naparsovani spusteni funkce, zatim bez parametru, to fakt nevim jak udelat
        name = self.pop(Lexer.IDENT)[1]
        self.pop("(")
        params = self.parseList()
        self.pop(")")
        return Functioncall(name, params)

    def parseComparison(self):
        """  COMPARISON ::= (kw_bigger | kw_eq | kw_smaller) """
        rhs = self.parseE1()
        op = self.pop()[0]
        lhs = self.parseE1()
        return BinaryOperator(lhs, rhs, op)

    def parseCondition(self):
        """ IF_STATEMENT ::= if op_paropen EXPRESSION op_parclose [ BLOCK ] [ else BLOCK ]
        !!!  CONDITION ::= kw_if   EXPRESSION COMPARISON EXPRESSION  BLOCK [ kw_else BLOCK ]!!!
        If podminka je klasicky podminka a za ni if, pripadne else block. Vsimnete si, ze oba dva jsou vlastne v moji gramatice nepovinne.
        """
        self.pop(Lexer.KW_IF)
        self.pop(Lexer.OP_PAROPEN)
        condition = self.parseComparison()
        self.pop(Lexer.OP_PARCLOSE)
        trueCase = Block() # to aby se nam chybeji vetev sprave zobrazila jako {}
        falseCase = Block() # to aby se nam chybeji vetev sprave zobrazila jako {}
        if (self.top()[0] == Lexer.OP_BRACEOPEN): # kdyz hned po podmince je {, vim ze je to if cast
            trueCase = self.parseBlock()
        if (self.top()[0] == Lexer.KW_ELSE): # jinak za ifem muze byt jeste else pro else cast
            self.pop()
            falseCase = self.parseBlock()
        # cokoli ostatniho by bylo za ifem, neni soucasti ifu
        return If(condition, trueCase, falseCase)

    def parseLoop(self):
        """ Podobne ifu, jen bez else casti """
        self.pop(Lexer.KW_WHILE)
        self.pop(Lexer.OP_PAROPEN)
        condition = self.parseComparison()
        self.pop(Lexer.OP_PARCLOSE)
        b = Block()
        if (self.top()[0] == Lexer.OP_BRACEOPEN):
            b = self.parseBlock()
        return While(condition, b)


    def parseBlock(self):
        """ BLOCK ::= op_braceopen { STATEMENT } op_braceclose

        Blok je podobny programu, proste nekolik prikazu za sebou.
        """
        self.pop(Lexer.OP_BRACEOPEN)
        result = Block()
        while (self.top()[0] != Lexer.OP_BRACECLOSE):
            result.add(self.parseStatement())
        self.pop(Lexer.OP_BRACECLOSE)
        return result



p = Parser(Lexer())
s = open("codeinput.txt", "r")
c = s.read()
p.lexer.analyzeString(c)


while (not p.lexer.isEOF()): # tohle slouzi k vypsani vsech tokenu
    print p.lexer._top, (p.lexer.popToken())
print p.lexer._tokens
p.lexer._top = 0
o = p.parse()
print o
print "################## predel parsovani a exec #################"
f = Frame(None)
print o.code
for node in o.code:
    if node != None:
        node.exe(f)
print program.functions